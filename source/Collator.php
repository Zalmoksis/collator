<?php

namespace Zalmoksis\Collator;

interface Collator {
    public function getSortKey(string $string): string;
}
